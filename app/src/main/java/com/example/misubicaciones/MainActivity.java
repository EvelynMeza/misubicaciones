package com.example.misubicaciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private FusedLocationProviderClient fusedLocationClient;
    DatabaseReference BaseDatos;
    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS;
    private EditText Edit1, Edit2, Edit3, Edit4, Edit5;
    private Button Btn1, Btn2,Btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Edit1 = (EditText) findViewById(R.id.editText);
        Edit2 = (EditText) findViewById(R.id.editText2);
        Edit3 = (EditText) findViewById(R.id.editText3);
        Edit4 = (EditText) findViewById(R.id.editText4);
        Edit5 = (EditText) findViewById(R.id.editText5);
        Btn1 = (Button)findViewById(R.id.button);
        Btn2 = (Button)findViewById(R.id.button2);
        Btn3 = (Button)findViewById(R.id.button3);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        BaseDatos = FirebaseDatabase.getInstance().getReference();
        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(MainActivity.this, "Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });
        MostrarRegistros();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            return;
        }
        SubirLatitudLon();
        Btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calcular();
            }
        });
        Btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });
        Btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "No me quedo profe :( Le juro que lo intente y no daba, no fue error mio por que en mi otra aplicacion de la actividad si me agarro :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void SubirLatitudLon() {

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.

                        if (location != null) {
                            Log.e("Latitud:",+location.getLatitude() + "Longitud: " +location.getLongitude());

                            Map<String,Object> posicion = new HashMap<>();

                            posicion.put("latitud", location.getLatitude());
                            posicion.put("longitud", location.getLongitude());
                            BaseDatos.child("usuarios").push().setValue(posicion);
                        }
                    }
                });
    }
    private void Calcular(){
        Location locationA = new Location("punto A");
        double latitud1 = Double.parseDouble(Edit1.getText().toString());
        double longitud1 = Double.parseDouble(Edit2.getText().toString());
        double latitud2 = Double.parseDouble(Edit3.getText().toString());
        double longitud2 = Double.parseDouble(Edit4.getText().toString());
        locationA.setLatitude(latitud1);
        locationA.setLongitude(longitud1);

        Location locationB = new Location("punto B");

        locationB.setLatitude(latitud2);
        locationB.setLongitude(longitud2);

        double distance = locationA.distanceTo(locationB);
        Toast.makeText(MainActivity.this, "La distancia es" + distance + "Metros" , Toast.LENGTH_SHORT).show();

    }
    private void MostrarRegistros() {
        BaseDatos.child("usuarios").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String cadena = "";

                for (DataSnapshot snapshot: dataSnapshot.getChildren()  ){
                    MapsEv registros = snapshot.getValue(MapsEv.class);


                    Double latitud= registros.getLatitud();
                    Double longitud = registros.getLongitud();
                    cadena = cadena + " Latitud: " + latitud +"\n Longitud: "+  longitud + "\n";
                }
               Edit5.setText(cadena);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
   
}
